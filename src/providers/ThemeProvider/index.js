import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'styled-components'

import defaultTheme from './palette'

const CustomThemeProvider = (props) => (
  <ThemeProvider theme={defaultTheme}>{props.children}</ThemeProvider>
)

CustomThemeProvider.propTypes = {
  children: PropTypes.node
}

CustomThemeProvider.defaultProps = {
  theme: defaultTheme
}

export default CustomThemeProvider
