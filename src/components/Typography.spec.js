import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import { WhiteText, YellowText } from './Typography'

describe('components/Typography', () => {
  it('WhiteText should render correctly', () => {
    const tree = mountWithTheme(<WhiteText>WhiteText</WhiteText>)

    expect(tree.find(WhiteText)).toMatchSnapshot()
  })

  it('YellowText should render correctly', () => {
    const tree = mountWithTheme(<YellowText>YellowText</YellowText>)

    expect(tree.find(YellowText)).toMatchSnapshot()
  })
})
