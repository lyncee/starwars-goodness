import styled from 'styled-components'

const DataHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 1rem;
  box-sizing: border-box;
`
export default DataHeader
