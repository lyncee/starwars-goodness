import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import DataPanel from './DataPanel'

describe('components/DataPanel', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<DataPanel>DataPanel</DataPanel>)

    expect(tree.find(DataPanel)).toMatchSnapshot()
  })
})
