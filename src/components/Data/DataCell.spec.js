import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import DataCell from './DataCell'

describe('components/DataCell', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<DataCell>DataCell</DataCell>)

    expect(tree.find(DataCell)).toMatchSnapshot()
  })

  it('should render correctly with noDesktopBorder', () => {
    const tree = mountWithTheme(<DataCell noDesktopBorder>DataCell</DataCell>)

    expect(tree.find(DataCell)).toMatchSnapshot()
  })

  it('should render correctly with noMobileBorder', () => {
    const tree = mountWithTheme(<DataCell noMobileBorder>DataCell</DataCell>)

    expect(tree.find(DataCell)).toMatchSnapshot()
  })
})
