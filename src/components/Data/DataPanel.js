import styled from 'styled-components'

const DataPanel = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme['gray9']};
  width: 100%;
  padding: 1.5rem;
  box-sizing: border-box;
  overflow-y: auto;
`
export default DataPanel
