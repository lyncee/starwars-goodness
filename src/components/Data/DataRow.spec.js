import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import DataRow from './DataRow'

describe('components/DataRow', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<DataRow>DataRow</DataRow>)

    expect(tree.find(DataRow)).toMatchSnapshot()
  })
})
