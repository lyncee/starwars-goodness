import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import DataHeader from './DataHeader'

describe('components/DataHeader', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<DataHeader>DataHeader</DataHeader>)

    expect(tree.find(DataHeader)).toMatchSnapshot()
  })
})
