import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import DataColumn from './DataColumn'

describe('components/DataColumn', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<DataColumn>DataColumn</DataColumn>)

    expect(tree.find(DataColumn)).toMatchSnapshot()
  })
})
