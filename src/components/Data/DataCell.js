import styled from 'styled-components'

const DataCell = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  padding: 1rem 0;
  box-sizing: border-box;
  border-bottom-width: 2px;
  border-bottom-color: ${(props) => props.theme['gray1']};
  border-bottom-style: ${(props) => (props.noMobileBorder ? 'none' : 'solid')};

  @media (min-width: 48rem) {
    border-bottom-style: ${(props) => props.noDesktopBorder ? 'none' : 'solid'};
  }
`
export default DataCell
