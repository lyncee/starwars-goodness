import styled from 'styled-components'

const DataRow = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  @media (min-width: 48rem) {
    flex-direction: row;

    & > :first-child {
      margin-right: 24px;
    }

    & > :not(:first-child):last-child {
      margin-left: 24px;
    }
  }
`
export default DataRow
