import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import { Panel } from './Panel'

describe('components/Panel', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<Panel />)

    expect(tree.find(Panel)).toMatchSnapshot()
  })
})
