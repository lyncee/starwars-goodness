import React from 'react'
import { mountWithTheme } from 'test/testHelper'

import TextInput from './TextInput'

describe('components/TextInput', () => {
  it('should render correctly', () => {
    const tree = mountWithTheme(<TextInput />)

    expect(tree.find(TextInput)).toMatchSnapshot()
  })

  it('should render correctly with width', () => {
    const tree = mountWithTheme(<TextInput width="250px" />)

    expect(tree.find(TextInput)).toMatchSnapshot()
  })
})
