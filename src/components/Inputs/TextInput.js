import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Input = styled.input.attrs({ type: 'text' })`
  display: block;
  width: ${(props) => props.width};
  height: auto;
  padding: 1rem 1.5rem;
  box-sizing: border-box;
  background: transparent;
  font-family: 'Roboto', sans-serif;
  font-size: 1.5rem;
  color: ${(props) => props.theme['white']};
  border: none;
  border-color: transparent;
  border-bottom: 2px solid  ${(props) => props.theme['white']};
  outline-width: 0;
  user-select: text;

  &::placeholder {
    color: ${(props) => props.theme['gray2']};
    font-size: 1.5rem;
  }
`

const TextInput = (props) => <Input {...props} />

TextInput.propTypes = {
  width: PropTypes.string
}

TextInput.defaultProps = {
  width: '100%'
}

export default TextInput
