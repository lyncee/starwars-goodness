import styled from 'styled-components'

const GenericText = styled.span`
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  font-size: ${(props) => props.size || '1.5rem'};
`

export const WhiteText = styled(GenericText)`
  color: ${(props) => props.theme['white']};
`

export const YellowText = styled(GenericText)`
  color: ${(props) => props.theme['yellow5']};
`
