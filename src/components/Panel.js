import styled from 'styled-components'

export const Panel = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
  background: ${(props) => props.theme['gray5']};
  border: 1px solid ${(props) => props.theme['gray7']};
  border-radius: 5px;
`
