import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { isNil } from 'ramda'

import { actions, selectors } from 'data'
import { DataCell } from 'components/Data'
import { HeartbeatLoader } from 'components/Loaders'
import { YellowText, WhiteText } from 'components/Typography'

const Wrapper = styled(DataCell)`
  display: flex;
  flex-direction: column;
  justify-content: ${(props) => props.align === 'center' ? 'center' : 'flex-start'};
  align-items: ${(props) => props.align === 'center' ? 'center' : 'flex-start'};
  min-height: 200px;
  cursor:pointer;

  & > :first-child {
    margin-bottom: 0.25rem;
  }
`

class Film extends React.PureComponent {
  componentDidMount () {
    this.props.actions.fetchFilm(this.props.id)
  }

  render () {
    const { loading, data } = this.props

    return loading || isNil(data) ? (
      <Wrapper align="center">
        <HeartbeatLoader />
      </Wrapper>
    ) : (
      <Wrapper onClick={this.handleToggle}>
        <YellowText>{data.title}</YellowText>
        <WhiteText size="1rem">{data.opening_crawl}</WhiteText>
      </Wrapper>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  data: selectors.film.selectData(ownProps.id)(state),
  loading: selectors.film.selectLoading(ownProps.id)(state)
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions.film, dispatch)
})

Film.propTypes = {
  actions: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Film)
