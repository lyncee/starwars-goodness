import React from 'react'
import PropTypes from 'prop-types'

import { DataPanel, DataHeader, DataColumn } from 'components/Data'
import { WhiteText } from 'components/Typography'
import { extractIds } from './services'
import Film from './Film'

const Filmography = ({ character }) => (
  <DataPanel>
    <DataColumn>
      <DataHeader>
        <WhiteText size="2rem">Filmography</WhiteText>
      </DataHeader>
      {extractIds(character.films).map((id) => <Film id={id} key={id} />)}
    </DataColumn>
  </DataPanel>
)

Filmography.propTypes = {
  character: PropTypes.object.isRequired
}

export default Filmography
