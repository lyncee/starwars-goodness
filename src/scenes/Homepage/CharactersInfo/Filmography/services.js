import { extractId } from 'services/url'

export const extractIds = (urls) => urls.map(extractId)
