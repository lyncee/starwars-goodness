import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { isEmpty } from 'ramda'

import Filmography from './Filmography'
import GeneralDetails from './GeneralDetails'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > :first-child {
    margin-right: 1rem;
  }

  & > last-child {
    margin-left: 1rem;
  }
`
const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  & > * {
    margin-bottom: 1rem;
  }
`
const CharactersInfo = ({ character }) => !isEmpty(character) ? (
  <Wrapper>
    <Column>
      <GeneralDetails character={character} />
    </Column>
    <Column>
      <Filmography character={character} />
    </Column>
  </Wrapper>
) : null

CharactersInfo.propTypes = {
  character: PropTypes.object
}

export default CharactersInfo
