import React from 'react'
import PropTypes from 'prop-types'

import { DataPanel, DataHeader, DataCell, DataColumn } from 'components/Data'
import { YellowText, WhiteText } from 'components/Typography'

const GeneralDetails = ({ character }) => (
  <DataPanel>
    <DataColumn>
      <DataHeader>
        <WhiteText size="2rem">General details</WhiteText>
      </DataHeader>
      <DataCell>
        <WhiteText>Name</WhiteText>
        <YellowText>{character.name}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Birth year</WhiteText>
        <YellowText>{character.birth_year}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Gender</WhiteText>
        <YellowText>{character.gender}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Height</WhiteText>
        <YellowText>{character.height}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Mass</WhiteText>
        <YellowText>{character.mass}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Hair color</WhiteText>
        <YellowText>{character.hair_color}</YellowText>
      </DataCell>
      <DataCell>
        <WhiteText>Eye color</WhiteText>
        <YellowText>{character.eye_color}</YellowText>
      </DataCell>
      <DataCell noDesktopBorder>
        <WhiteText>Skin color</WhiteText>
        <YellowText>{character.skin_color}</YellowText>
      </DataCell>
    </DataColumn>
  </DataPanel>
)

GeneralDetails.propTypes = {
  character: PropTypes.object.isRequired
}

export default GeneralDetails
