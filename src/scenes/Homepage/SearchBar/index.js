import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { TextInput } from 'components/Inputs'
import Results from './Results'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 80%;
`
const SearchInput = styled(TextInput)`
  padding-right: 4rem;
`

class SearchBar extends React.Component {
  constructor (props) {
    super(props)
    this.searchBarRef = React.createRef()
    this.state = { search: '' }
    this.handleChange = this.handleChange.bind(this)
    this.handleSelectCharacter = this.handleSelectCharacter.bind(this)
    this.handleToggleSearch = this.handleToggleSearch.bind(this)
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleToggleSearch)
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleToggleSearch)
  }

  handleChange (e) {
    this.setState({ search: e.target.value })
  }

  handleSelectCharacter (c) {
    this.props.selectCharacter(c)
    this.setState({ search: '' })
  }

  handleToggleSearch (e) {
    if (!this.searchBarRef.current.contains(e.target)) {
      this.setState({ search: '' })
    }
  }

  render () {
    const { search } = this.state

    return (
      <Wrapper ref={this.searchBarRef}>
        <SearchInput onChange={this.handleChange} placeholder="Search for your favorites Star Wars character(s)..." value={search} />
        <Results search={search} selectCharacter={this.handleSelectCharacter} />
      </Wrapper>
    )
  }
}

SearchBar.propTypes = {
  selectCharacter: PropTypes.func.isRequired
}

export default SearchBar
