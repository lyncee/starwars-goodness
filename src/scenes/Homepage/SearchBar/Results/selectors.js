import { createSelector } from 'reselect'
import { contains, filter, isEmpty, prop, toLower } from 'ramda'
import { selectors } from 'data'

export const filterResults = (search, state) => createSelector(
  selectors.character.selectData,
  (characters) => isEmpty(search) ? [] : filter(r => contains(toLower(search), toLower(prop('name', r))), characters)
)(state)
