import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'

import { WhiteText } from 'components/Typography'
import { filterResults } from './selectors'

const Wrapper = styled.div`
  position: absolute;
  top: 65px;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  max-height: 300px;
  border: 2px solid ${(props) => props.theme['gray5']};
  overflow-y: auto;
`
const ResultItem = styled(WhiteText)`
  width: 100%;
  padding: 0.5rem;
  box-sizing: border-box;
  background: ${(props) => props.theme['gray9']};
  cursor: pointer;

  &:hover { 
    background: ${(props) => props.theme['gray7']};
  }
`

const Results = ({ characters, selectCharacter }) => !isEmpty(characters) ? (
  <Wrapper>
    {characters.map(c => <ResultItem key={c.name} onClick={() => selectCharacter(c)} size="1.5rem">{c.name}</ResultItem>)}
  </Wrapper>
) : null

Results.propTypes = {
  characters: PropTypes.array,
  selectCharacter: PropTypes.func.isRequired
}

Results.defaultProps = {
  characters: []
}

const mapStateToProps = (state, { search }) => ({
  characters: filterResults(search, state)
})

export default connect(mapStateToProps)(Results)
