
export const computePercentage = (number, total) => Math.round((number / total) * 100)
