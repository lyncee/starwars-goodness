import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { length } from 'ramda'

import { selectors } from 'data'
import { computePercentage } from './services'
import { WhiteText } from 'components/Typography'

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 30px;
  text-align: center;
`
const Container = styled.div`
  width: ${(props) => props.width};
  height: 100%;
  background: ${(props) => props.theme['red10']};
  transition: 0.3s;
`
const Indicator = styled(WhiteText)`
  position: absolute;
  top: 0;
  left: 50%;
  width: 50px;
  height: 30px;
  margin-left: -25px;
  margin-top: 5px;
`

const LoadingBar = ({ characters, total }) => {
  const percentageValue = computePercentage(length(characters), total)
  const percentage = isNaN(percentageValue) ? '0%' : `${percentageValue}%`

  return (
    <Wrapper>
      <Container width={percentage} />
      <Indicator size="1rem">{percentage}</Indicator>
    </Wrapper>
  )
}

LoadingBar.propTypes = {
  characters: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired
}

const mapStateToProps = (state) => ({
  characters: selectors.character.selectData(state),
  total: selectors.character.selectCount(state)
})

export default connect(mapStateToProps)(LoadingBar)
