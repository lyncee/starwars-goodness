import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions } from 'data'
import CharactersInfo from './CharactersInfo'
import LoadingBar from './LoadingBar'
import SearchBar from './SearchBar'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: 100%;
`
const SearchArea = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 80%;
  height: 200px;
`
const ResultArea = styled.div`
  width: 100%;
  height: calc(100% - 200px);
`

class Homepage extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = { character: {} }
    this.selectCharacter = this.selectCharacter.bind(this)
  }

  componentDidMount () {
    this.props.actions.fetchCharacters()
  }

  selectCharacter (character) {
    this.setState({ character })
  }

  render () {
    const { character } = this.state

    return (
      <Wrapper>
        <LoadingBar />
        <SearchArea>
          <SearchBar selectCharacter={this.selectCharacter} />
        </SearchArea>
        <ResultArea>
          <CharactersInfo character={character} />
        </ResultArea>
      </Wrapper>
    )
  }
}

Homepage.propTypes = {
  actions: PropTypes.object.isRequired
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions.character, dispatch)
})

export default connect(undefined, mapDispatchToProps)(Homepage)
