import React from 'react'
import ReactDOM from 'react-dom'
import App from './app.js'
import configureStore from 'store'

const { store, history } = configureStore()

ReactDOM.render(
  <App history={history} store={store} />,
  document.getElementById('app')
)
