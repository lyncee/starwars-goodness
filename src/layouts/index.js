import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Route } from 'react-router-dom'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  min-height: 100%;
  background: ${(props) => props.theme['black']};
`
const Container = styled.div`
  display: flex;
  width: 100%;

  @media(min-width: 48rem) {
    width: 1200px;
  }
`
class Layout extends React.PureComponent {
  constructor (props) {
    super(props)
    this.renderProps = this.renderProps.bind(this)
  }

  renderProps () {
    const { component: Component, ...rest } = this.props
    return (
      <Wrapper>
        <Container>
          <Component {...rest} />
        </Container>
      </Wrapper>
    )
  }

  render () {
    return <Route render={this.renderProps} />
  }
}

Layout.propTypes = {
  component: PropTypes.node
}

export default Layout
