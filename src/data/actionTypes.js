import * as character from './character/actionTypes'
import * as film from './film/actionTypes'

export {
  character,
  film
}
