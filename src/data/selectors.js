import * as character from './character/selectors'
import * as film from './film/selectors'

export {
  character,
  film
}
