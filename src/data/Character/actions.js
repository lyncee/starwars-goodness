import * as AT from './actionTypes'

export const fetchCharacters = () => ({ type: AT.FETCH_CHARACTERS })

export const fetchCharactersInit = (count) => ({ payload: count, type: AT.FETCH_CHARACTERS_INIT })

export const fetchCharactersSave = (data) => ({ payload: data, type: AT.FETCH_CHARACTERS_SAVE })

export const fetchCharactersSuccess = () => ({ type: AT.FETCH_CHARACTERS_SUCCESS })

export const fetchCharactersError = (error) => ({ payload: error, type: AT.FETCH_CHARACTERS_ERROR })
