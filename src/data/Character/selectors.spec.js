import * as S from './selectors'

const state = {
  character: {
    count: 5,
    data: [1, 2, 3, 4, 5],
    error: '',
    loading: false
  }
}

describe('data/character/selectors', () => {
  it('should return count', () => {
    const result = S.selectCount(state)
    const expectedResult = 5
    expect(result).toEqual(expectedResult)
  })

  it('should return data', () => {
    const result = S.selectData(state)
    const expectedResult = [1, 2, 3, 4, 5]
    expect(result).toEqual(expectedResult)
  })

  it('should return loading', () => {
    const result = S.selectLoading(state)
    const expectedResult = false
    expect(result).toEqual(expectedResult)
  })
})
