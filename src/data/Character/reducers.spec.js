import reducer from './reducers'
import * as AT from './actionTypes'

describe('data/character/reducers', () => {
  it('should return the initial state', () => {
    const initialState = {
      count: 0,
      data: [],
      error: '',
      loading: false
    }
    const action = {}
    const expectedState = initialState
    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle FETCH_CHARACTERS', () => {
    const initialState = {
      count: 0,
      data: [],
      error: '',
      loading: false
    }
    const action = { type: AT.FETCH_CHARACTERS }
    const expectedState = {
      count: 0,
      data: [],
      error: '',
      loading: true
    }
    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle FETCH_CHARACTERS_INIT', () => {
    const initialState = {
      count: 0,
      data: [],
      error: '',
      loading: false
    }
    const action = { payload: 3, type: AT.FETCH_CHARACTERS_INIT }
    const expectedState = {
      count: 3,
      data: [],
      error: '',
      loading: false
    }
    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle FETCH_CHARACTERS_SAVE', () => {
    const initialState = {
      count: 0,
      data: [1, 2, 3],
      error: '',
      loading: false
    }
    const action = { payload: [4, 5, 6], type: AT.FETCH_CHARACTERS_SAVE }
    const expectedState = {
      count: 0,
      data: [1, 2, 3, 4, 5, 6],
      error: '',
      loading: false
    }
    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle FETCH_CHARACTERS_SUCCESS', () => {
    const initialState = {
      count: 0,
      data: [],
      error: '',
      loading: true
    }
    const action = { type: AT.FETCH_CHARACTERS_SUCCESS }
    const expectedState = {
      count: 0,
      data: [],
      error: '',
      loading: false
    }
    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle FETCH_CHARACTERS_ERROR', () => {
    const initialState = {
      count: 0,
      data: [],
      error: '',
      loading: true
    }
    const action = { payload: 'Something wrong !', type: AT.FETCH_CHARACTERS_ERROR }
    const expectedState = {
      count: 0,
      data: [],
      error: 'Something wrong !',
      loading: false
    }
    expect(reducer(initialState, action)).toEqual(expectedState)
  })
})
