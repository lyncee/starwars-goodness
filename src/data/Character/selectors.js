import { path } from 'ramda'

export const selectCount = path(['character', 'count'])

export const selectData = path(['character', 'data'])

export const selectLoading = path(['character', 'loading'])
