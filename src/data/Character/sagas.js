import { call, put, takeEvery } from 'redux-saga/effects'
import { prop } from 'ramda'

import { getCharacters } from 'services/api'
import * as AT from './actionTypes'
import * as A from './actions'

const fetchCharacters = function * (action) {
  try {
    const page1 = yield call(getCharacters)
    const totalCharacters = prop('count', page1)
    const totalPages = Math.ceil(totalCharacters / 10)
    yield put(A.fetchCharactersInit(totalCharacters))
    yield put(A.fetchCharactersSave(prop('results', page1)))
    for (let page = 2; page <= totalPages; page++) {
      const otherPage = yield call(getCharacters, page)
      yield put(A.fetchCharactersSave(otherPage.results))
    }
    yield put(A.fetchCharactersSuccess())
  } catch (e) {
    yield put(A.fetchCharactersError(e.message))
  }
}

export default function * () {
  yield takeEvery(AT.FETCH_CHARACTERS, fetchCharacters)
}
