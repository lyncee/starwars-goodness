import { assoc, compose, concat, prop } from 'ramda'
import * as AT from './actionTypes'

const INITIAL_STATE = {
  count: 0,
  data: [],
  error: '',
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  const { payload, type } = action

  switch (type) {
    case AT.FETCH_CHARACTERS: {
      return assoc('loading', true, state)
    }

    case AT.FETCH_CHARACTERS_INIT: {
      return assoc('count', payload, state)
    }

    case AT.FETCH_CHARACTERS_SAVE: {
      const currentCharacters = prop('data', state)
      return assoc('data', concat(currentCharacters, payload), state)
    }

    case AT.FETCH_CHARACTERS_SUCCESS: {
      return assoc('loading', false, state)
    }

    case AT.FETCH_CHARACTERS_ERROR: {
      return compose(
        assoc('error', payload),
        assoc('loading', false)
      )(state)
    }
    default: {
      return state
    }
  }
}
