import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import character from './character/reducers'
import film from './film/reducers'

export default (history) => combineReducers({
  character,
  film,
  router: connectRouter(history)
})
