import { all, fork } from 'redux-saga/effects'
import character from './character/sagas.js'
import film from './film/sagas.js'

const sagas = function * () {
  yield all([
    fork(character),
    fork(film)
  ])
}

export default sagas
