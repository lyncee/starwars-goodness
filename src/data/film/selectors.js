import { curry, path } from 'ramda'

export const selectData = curry((id, state) => path(['film', id, 'data'], state))

export const selectError = curry((id, state) => path(['film', id, 'error'], state))

export const selectLoading = curry((id, state) => path(['film', id, 'loading'], state))
