import * as AT from './actionTypes'

export const fetchFilm = (id) => ({ payload: id, type: AT.FETCH_FILM })

export const fetchFilmSuccess = (id, data) => ({ payload: { data, id }, type: AT.FETCH_FILM_SUCCESS })

export const fetchFilmError = (id, error) => ({ payload: { error, id }, type: AT.FETCH_FILM_ERROR })
