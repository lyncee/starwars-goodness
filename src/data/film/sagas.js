import { call, put, select, takeEvery } from 'redux-saga/effects'
import { isEmpty } from 'ramda'

import { getFilm } from 'services/api'
import * as AT from './actionTypes'
import * as A from './actions'
import * as S from './selectors'

const fetchFilm = function * (action) {
  const id = action.payload
  const data = yield select(S.selectData(id))
  if (isEmpty(data)) {
    try {
      const data = yield call(getFilm, id)
      yield put(A.fetchFilmSuccess(id, data))
    } catch (e) {
      yield put(A.fetchFilmError(id, e.message))
    }
  }
}

export default function * () {
  yield takeEvery(AT.FETCH_FILM, fetchFilm)
}
