import { assocPath, compose } from 'ramda'
import * as AT from './actionTypes'

const INITIAL_STATE = {}

export default (state = INITIAL_STATE, action) => {
  const { payload, type } = action

  switch (type) {
    case AT.FETCH_FILM: {
      const id = payload
      return compose(
        assocPath([id, 'data'], {}),
        assocPath([id, 'loading'], true),
        assocPath([id, 'error'], '')
      )(state)
    }
    case AT.FETCH_FILM_SUCCESS: {
      const { id, data } = payload
      return compose(
        assocPath([id, 'data'], data),
        assocPath([id, 'loading'], false)
      )(state)
    }
    case AT.FETCH_FILM_ERROR: {
      const { id, error } = payload
      return compose(
        assocPath([id, 'error'], error),
        assocPath([id, 'loading'], false)
      )(state)
    }
    default: {
      return state
    }
  }
}
