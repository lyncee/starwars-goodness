import * as character from './character/actions'
import * as film from './film/actions'
import { routerActions as router } from 'connected-react-router'

export {
  character,
  film,
  router
}
