import React from 'react'
import PropTypes from 'prop-types'
import { Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import ThemeProvider from 'providers/ThemeProvider'

import Layout from 'layouts'
import Homepage from 'scenes/Homepage'

const App = ({ history, store }) => (
  <ThemeProvider>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Layout component={Homepage} path="/" />
        </Switch>
      </ConnectedRouter>
    </Provider>
  </ThemeProvider>
)

App.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
}

export default App
