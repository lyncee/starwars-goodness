export const extractId = (url) => {
  const matchs = url.match(/.*\/([0-9])\/*$/)
  return matchs ? matchs[1] : undefined
}
