import fetch from 'isomorphic-fetch'
import 'es6-promise'
import settings from '../settings'

const query = (method, endpoint, data = null) => {
  let options = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    method
  }

  if (data) {
    options.body = JSON.stringify(data)
  }

  return fetch(`${settings.API_DOMAIN}${endpoint}`, options)
    .then((r) => (r.ok ? Promise.resolve(r) : Promise.reject(r)))
    .then((r) => r.json())
}

const get = (endpoint) => query('get', endpoint)

export { get }
