import { extractId } from './url'

describe('services/url', () => {
  it('extractId should return correct id when an id is present in the url', () => {
    const expectedResult = '3'
    const result = extractId('https://swapi.co/people/3/')

    expect(result).toEqual(expectedResult)
  })

  it('extractId should return undefined when there is no id present in the url', () => {
    const expectedResult = undefined
    const result = extractId('https://swapi.co/people/')

    expect(result).toEqual(expectedResult)
  })
})
