import { get } from './query'

const getCharacters = (page = 1) => get(`/people/?page=${page}`)

const getFilm = (id) => get(`/films/${id}/`)

export {
  getCharacters,
  getFilm
}
