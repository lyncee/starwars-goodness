# starwars-goodness

## Introduction

Please install yarn (if needed) prior to running the different commands below:
* `npm install -g yarn`

## Commands

* `yarn`: restore dependencies
* `yarn start`: start project on localhost:8080
* `yarn test`: run tests
* `yarn test:update`: run tests while updating snapshots