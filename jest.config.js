const coverageEnabled = process.argv.indexOf('--coverage') !== -1

module.exports = {
  clearMocks: true,
  collectCoverage: coverageEnabled,
  collectCoverageFrom: [
    'components/**/**.js',
    'features/**/**.js',
    'layouts/**/**.js',
    'modals/**/**.js',
    'pages/**/**.js',
    'providers/**/**.js',
    'server/**/**.js',
    'services/**/**.js'
  ],
  coverageDirectory: './coverage',
  coverageReporters: ['json-summary', 'lcov'].filter(Boolean),
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
  rootDir: __dirname,
  setupFiles: ['<rootDir>/test/enzyme.js', '<rootDir>/test/mocks.js'],
  transform: {
    '^.+\\.(js)$': '<rootDir>/node_modules/babel-jest'
  },
  verbose: true
}
