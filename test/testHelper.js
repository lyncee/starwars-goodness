import { mount } from 'enzyme'
import { ThemeConsumer } from 'styled-components'
import theme from 'providers/ThemeProvider/palette'

export const mountWithTheme = (children) => {
  ThemeConsumer._currentValue = theme
  return mount(children)
}
